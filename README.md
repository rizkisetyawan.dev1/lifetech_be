# Setup Project

Create .env file

```
DATABASE_URL="mysql://root:secret@localhost:3306/db_dans"
```

```shell

npm install

npx prisma migrate dev

npx prisma generate

npm run build

npm run start

```
