# User API Spec

## Register User

Endpoint : POST /api/users

Request Body :

```json
{
  "username": "rizki",
  "password": "secret",
  "name": "Rizki Setyawan"
}
```

Response Body (Success):

```json
{
  "data": {
    "username": "rizki",
    "name": "Rizki Setyawan"
  }
}
```

Response Body (Failed):

```json
{
  "errors": {
    "password": "String must contain at least 1 character(s)"
  }
}
```

## Login User

Endpoint : POST /api/users/login

Request Body :

```json
{
  "username": "rizki",
  "password": "secret"
}
```

Response Body (Success):

```json
{
  "data": {
    "username": "rizki",
    "name": "Rizki Setyawan",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."
  }
}
```

Response Body (Failed):

```json
{
  "errors": "username or password wrong ..."
}
```

## Get User

Endpoint : GET /api/users/current

Request Header :

- Authorization : Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...

Response Body (Success) :

```json
{
  "data": {
    "username": "rizki",
    "name": "Rizki Setyawan"
  }
}
```

Response Body (Failed) :

```json
{
  "errors": "Unauthorized, ..."
}
```
