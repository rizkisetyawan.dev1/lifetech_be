## Search Contact

Endpoint : GET /api/jobs

Query Parameter :

- description : string, optional
- location : string, optional
- full_time : string ( true or false ), optional
- page : number, default 1

Request Header :

- Authorization : Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...

Response Body (Success) :

```json
{
  "data": [
    {
      "id": "2e99bd71-9684-4182-bd59-28a634ec9dd7",
      "type": "Full Time",
      "url": "https://jobs.github.com/positions/2e99bd71-9684-4182-bd59-28a634ec9dd7",
      "created_at": "Mon May 17 11:51:58 UTC 2021",
      "company": "Mimi Hearing Technologies GmbH",
      "company_url": "https://www.mimi.io/",
      "location": "Berlin",
      "title": "Senior Android Developer (f/m/d) (80-100%)",
      "description": "<p>Our mission at ..",
      "how_to_apply": "<p><a href=\"https://t.gohiring.com/h/n..",
      "company_logo": "https://jobs.github.com/rails/active_storage.."
    },
    {
      "id": "96f19efd-1a17-4375-9aa6-17a75e9570aa",
      "type": "Full Time",
      "url": "https://jobs.github.com/positions/96f19efd-1a17-4375-9aa6-17a75e9570aa",
      "created_at": "Mon May 17 11:10:09 UTC 2021",
      "company": "OSI International Holding GmbH",
      "company_url": "https://de.osigroup.com/",
      "location": "86368",
      "title": "Architect IT Applications (m/w/d)",
      "description": "<p><strong>OSI in Kürze</strong></p>..",
      "how_to_apply": "<p><a href=\"https://t.gohiring.com/h/..",
      "company_logo": "https://jobs.github.com.."
    }
  ]
}
```

Response Body (Failed) :

```json
{
  "errors": "Unauthorized"
}
```

## Get Job

Endpoint : GET /api/jobs/:jobId

Request Header :

- Authorization : Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...

Response Body (Success) :

```json
{
  "data": {
    "id": "2e99bd71-9684-4182-bd59-28a634ec9dd7",
    "type": "Full Time",
    "url": "https://jobs.github.com/positions/2e99bd71-9684-4182-bd59-28a634ec9dd7",
    "created_at": "Mon May 17 11:51:58 UTC 2021",
    "company": "Mimi Hearing Technologies GmbH",
    "company_url": "https://www.mimi.io/",
    "location": "Berlin",
    "title": "Senior Android Developer (f/m/d) (80-100%)",
    "description": "<p>Our mission at ..",
    "how_to_apply": "<p><a href=\"https://t.gohiring.com/h/n..",
    "company_logo": "https://jobs.github.com/rails/active_storage.."
  }
}
```

Response Body (Failed) :

```json
{
  "errors": "Unauthorized"
}
```
