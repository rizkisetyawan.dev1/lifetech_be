import { z, ZodType } from 'zod';

export class UserValidation {

  static readonly CREATE: ZodType = z.object({
    username: z.string().min(1).max(100),
    email: z.string().email(),
    name: z.string().min(1).max(100),
    country: z.string().min(1).max(100),
  });

  static readonly UPDATE: ZodType = z.object({
    id: z.string().min(1).max(100),
    username: z.string().min(1).max(100),
    email: z.string().email(),
    name: z.string().min(1).max(100),
    country: z.string().min(1).max(100),
  });

  static readonly LOGIN: ZodType = z.object({
    username: z.string().min(1).max(100),
    password: z.string().min(1).max(100)
  });
};
