import { z, ZodType } from "zod";

export class JobValidation {

  static readonly CREATE: ZodType = z.object({
    type: z.string(),
    company: z.string(),
    location: z.string(),
    title: z.string(),
    description: z.string(),
    how_to_apply: z.string(),
    company_logo: z.string().optional(),
  });

  static readonly GET: ZodType = z.object({
    jobId: z.string(),
  });

  static readonly UPDATE: ZodType = z.object({
    id: z.string(),
    type: z.string(),
    company: z.string(),
    location: z.string(),
    title: z.string(),
    description: z.string(),
    how_to_apply: z.string(),
    company_logo: z.string().optional(),
  });

}
