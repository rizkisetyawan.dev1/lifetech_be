import { NextFunction, Request, Response } from "express";
const XLSX = require('xlsx');


export class XlsxController {
  static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const data = req.body;
      if (!Array.isArray(data) || data.length === 0) {
        return res.status(400).send('Invalid data format');
      }

      const wb = XLSX.utils.book_new();
      const ws = XLSX.utils.json_to_sheet(data);
      XLSX.utils.book_append_sheet(wb, ws, "Data");
      const buffer = XLSX.write(wb, { type: 'buffer', bookType: 'xlsx' });

      res.setHeader('Content-Disposition', 'attachment; filename=data.xlsx');
      res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      res.send(Buffer.from(buffer));
    } catch (error) {
      res.status(500).send('Internal Server Error');
    }
  }
}