import { NextFunction, Request, Response } from "express";
import { JobService } from "../service/job-service";
import { CreateJobRequest, UpdateJobRequest } from "../model/job-model";

export class JobController {

  static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const request: CreateJobRequest = req.body as CreateJobRequest;
      const response = await JobService.create(request);

      res.status(200).json({
        data: response
      });
    } catch (e) {
      next(e);
    }
  }

  static async list(req: Request, res: Response, next: NextFunction) {
    try {
      const response = await JobService.list();
      res.status(200).json({
        data: response
      });
    } catch (e) {
      next(e);
    }
  }

  static async get(req: Request, res: Response, next: NextFunction) {
    try {
      const { jobId } = req.params;
      const response = await JobService.get(jobId);
      res.status(200).json({
        data: response
      });
    } catch (e) {
      next(e);
    }
  }

  static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const request: UpdateJobRequest = req.body as UpdateJobRequest;

      request.id = req.params.jobId;

      const response = await JobService.update(request);
      res.status(200).json({
        data: response
      });
    } catch (e) {
      next(e);
    }
  }

  static async remove(req: Request, res: Response, next: NextFunction) {
    try {
      const { jobId } = req.params;
      await JobService.remove(jobId);
      res.status(200).json({
        data: "OK"
      });
    } catch (e) {
      next(e);
    }
  }
};
