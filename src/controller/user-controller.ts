import { NextFunction, Request, Response } from "express";
import { CreateUserRequest, UpdateUserRequest } from "../model/user-model";
import { UserService } from "../service/user-service";

export class UserController {

  static async create(req: Request, res: Response, next: NextFunction) {
    try {
      const request: CreateUserRequest = req.body as CreateUserRequest;
      const response = await UserService.create(request);
      res.status(200).json({
        data: response
      })
    } catch (e) {
      next(e);
    }
  }

  static async list(req: Request, res: Response, next: NextFunction) {
    try {
      const response = await UserService.list();
      res.status(200).json({
        data: response
      });
    } catch (e) {
      next(e);
    }
  }

  static async update(req: Request, res: Response, next: NextFunction) {
    try {
      const request: UpdateUserRequest = req.body as UpdateUserRequest;
      request.id = req.params.userId;

      const response = await UserService.update(request);
      res.status(200).json({
        data: response
      });
    } catch (e) {
      next(e);
    }
  }

  static async remove(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.params;
      await UserService.remove(userId);
      res.status(200).json({
        data: "OK"
      });
    } catch (e) {
      next(e);
    }
  }

}