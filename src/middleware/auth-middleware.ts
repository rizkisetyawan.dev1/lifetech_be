import { NextFunction, Response } from "express";
import jwt from "jsonwebtoken";
import { prismaClient } from "../application/database";
import { UserRequest } from "../type/user-request";

export const authMiddleware = async (req: UserRequest, res: Response, next: NextFunction) => {
  let token = req.get('Authorization');

  if (token && token.startsWith('Bearer ')) {
    try {
      token = token.substring(7);
      const decodedToken = jwt.verify(token, "secret_key") as { username: string };

      // Find user in database using username from decoded token
      const user = await prismaClient.user.findFirst({
        where: {
          username: decodedToken.username
        }
      });

      if (user) {
        req.user = user;
        next();
        return;
      }
    } catch (error) {
      // If JWT token verification fails, send 401 Unauthorized response
      res.status(401).json({
        errors: error
      }).end();
      return;
    }
  }

  // If no token provided, send 401 Unauthorized response
  res.status(401).json({
    errors: "Unauthorized"
  }).end();
}

