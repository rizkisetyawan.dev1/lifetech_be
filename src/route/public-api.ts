import express from "express";
import { UserController } from "../controller/user-controller";
import { JobController } from "../controller/job-controller";
import { XlsxController } from "../controller/xlsx-controller";

export const publicRouter = express.Router();

// Use APi
publicRouter.post("/api/users", UserController.create);
publicRouter.get("/api/users", UserController.list);
publicRouter.put("/api/users/:userId", UserController.update);
publicRouter.delete("/api/users/:userId", UserController.remove);

// Job APi
publicRouter.get("/api/jobs", JobController.list);
publicRouter.post("/api/jobs", JobController.create);
publicRouter.get("/api/jobs/:jobId", JobController.get);
publicRouter.put("/api/jobs/:jobId", JobController.update);
publicRouter.delete("/api/jobs/:jobId", JobController.remove);

// Xlsx API
publicRouter.post("/api/xlsx", XlsxController.create);