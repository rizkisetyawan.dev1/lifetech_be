import { prismaClient } from "../application/database";
import { ResponseError } from "../error/response-error";
import { CreateUserRequest, UpdateUserRequest, UserResponse, toUserResponse } from "../model/user-model";
import { UserValidation } from "../validation/user-validation";
import { Validation } from "../validation/validation";

export class UserService {
  static async checkUsername(id: string, username: string) {
    const usernameExist = await prismaClient.user.findFirst({
      where: {
        username: username,
        id: {
          not: id
        }
      }
    });

    if (usernameExist) {
      throw new ResponseError(404, "username already exist");
    }
  }

  static async checkEmail(id: string, email: string) {
    const emailExist = await prismaClient.user.findFirst({
      where: {
        email: email,
        id: {
          not: id
        }
      }
    });

    if (emailExist) {
      throw new ResponseError(404, "email already exist");
    }
  }

  static async create(request: CreateUserRequest): Promise<UserResponse> {
    const createRequest = Validation.validate(UserValidation.CREATE, request);
    await this.checkEmail('', createRequest.email);
    await this.checkUsername('', createRequest.username);
    const user = await prismaClient.user.create({
      data: createRequest
    });
    return toUserResponse(user);
  }

  static async list(): Promise<Array<UserResponse>> {
    const users = await prismaClient.user.findMany({
      orderBy: {
        created_at: 'desc'
      }
    });

    return users.map((user) => toUserResponse(user));
  }

  static async update(request: UpdateUserRequest): Promise<UserResponse> {
    const updateRequest = Validation.validate(UserValidation.UPDATE, request);
    await this.checkEmail(updateRequest.id, updateRequest.email);
    await this.checkUsername(updateRequest.id, updateRequest.username);

    const user = await prismaClient.user.update({
      where: {
        id: updateRequest.id,
      },
      data: updateRequest
    });
    return toUserResponse(user);
  }

  static async remove(userId: string): Promise<UserResponse> {
    const job = await prismaClient.user.delete({
      where: {
        id: userId
      }
    });
    return toUserResponse(job);
  }
};
