import axios from 'axios';
import { CreateJobRequest, JobResponse, UpdateJobRequest, toJobResponse } from "../model/job-model";
import { prismaClient } from '../application/database';
import { Validation } from '../validation/validation';
import { JobValidation } from '../validation/job-validation';
import { Job } from '@prisma/client';
import { ResponseError } from '../error/response-error';

export class JobService {

  static async create(request: CreateJobRequest): Promise<JobResponse> {
    const createRequest = Validation.validate(JobValidation.CREATE, request);
    const contact = await prismaClient.job.create({
      data: createRequest
    });
    return toJobResponse(contact);
  }

  static async list(): Promise<Array<JobResponse>> {
    const jobs = await prismaClient.job.findMany();

    return jobs.map((job) => toJobResponse(job));
  }

  static async checkJobMustExists(jobId: string): Promise<Job> {
    const job = await prismaClient.job.findFirst({
      where: {
        id: jobId,
      }
    });

    if (!job) {
      throw new ResponseError(404, "Job is not found");
    }

    return job;
  }

  static async get(jobId: string): Promise<JobResponse> {
    const job = await this.checkJobMustExists(jobId);

    return toJobResponse(job);
  }

  static async update(request: UpdateJobRequest): Promise<JobResponse> {
    const updateRequest = Validation.validate(JobValidation.UPDATE, request);

    await this.checkJobMustExists(updateRequest.id);

    const job = await prismaClient.job.update({
      where: {
        id: updateRequest.id,
      },
      data: updateRequest
    })

    return toJobResponse(job);
  }

  static async remove(jobId: string): Promise<JobResponse> {

    await this.checkJobMustExists(jobId);

    const job = await prismaClient.job.delete({
      where: {
        id: jobId
      }
    });
    return toJobResponse(job);
  }

};
