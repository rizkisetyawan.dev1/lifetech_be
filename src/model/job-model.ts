import { Job } from "@prisma/client";

export type JobResponse = {
  id: string;
  type: string;
  created_at: Date;
  company: string;
  location: string;
  title: string;
  description: string;
  how_to_apply: string;
  company_logo: string | null;
}

export type CreateJobRequest = {
  type: string;
  company: string;
  location: string;
  title: string;
  description: string;
  how_to_apply: string;
  company_logo: string | null;
}


export type UpdateJobRequest = {
  id: string;
  type: string;
  company: string;
  location: string;
  title: string;
  description: string;
  how_to_apply: string;
  company_logo: string | null;
}

export function toJobResponse(job: Job): JobResponse {
  return {
    id: job.id,
    type: job.type,
    created_at: job.created_at,
    company: job.company,
    location: job.location,
    title: job.title,
    description: job.description,
    how_to_apply: job.how_to_apply,
    company_logo: job.company_logo,
  }
}
