import { User } from "@prisma/client";

export type UserResponse = {
  id: string;
  username: string;
  email: string;
  name: string;
  country: string;
  created_at: Date;
};

export type CreateUserRequest = {
  username: string;
  email: string;
  name: string;
  country: string;
};

export type UpdateUserRequest = {
  id: string;
  username: string;
  email: string;
  name: string;
  country: string;
};


export function toUserResponse(user: User): UserResponse {
  return {
    id: user.id,
    username: user.username,
    email: user.email,
    name: user.name,
    country: user.country,
    created_at: user.created_at,
  }
}
